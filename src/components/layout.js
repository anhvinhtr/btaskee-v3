import * as React from 'react'
import Header from './header'
import Footer from './footer'
import 'bootstrap/dist/css/bootstrap.min.css'
import './layout.module.css';

const Layout = ({ pageTitle, children }) => {
  return (
    <div>
      <title>{pageTitle}</title>
      <Header></Header>
      <main>
        {children}
      </main>
      <Footer></Footer>
    </div>
  )
}

export default Layout