import * as React from 'react'
import logo from '../images/btaskee-logo-update.png'
import appStore from '../images/icon-download-app-store.png'
import playStore from '../images/icon-download-play-store.png'
import { Container, Row, Col } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

const Footer = () => {
    return (
        <div style={{'fontSize':'14px','backgroundColor':'#F9F9F9'}} className='pt-5 mt-5'>
            <Container bg="dark" variant="dark">
                <Row>
                    <Col xs={12} md={3}>
                        <img
                            src={logo}
                            alt='Logo bTaskee'
                            width="120px"
                            className="mb-3"
                            // height="30"
                        />
                        <p>Công Ty TNHH bTaskee<br/>
                        284/25/20 Lý Thường Kiệt, Phường 14, Quận 10, Tp.Hồ Chí Minh 72506<br/>
                        Mã số doanh nghiệp: 0313723825<br/>
                        Đại Diện Công Ty: Ông Đỗ Đắc Nhân Tâm<br/>
                        Chức vụ: Giám Đốc<br/>
                        Số điện thoại: 1900 636 736<br/>
                        Email: support@btaskee.com</p>
                        <div class="dmcalogo">
                            <a href="//www.dmca.com/Protection/Status.aspx?ID=89fbb0f1-479b-42c1-988f-ea4e67a69268" title="DMCA.com Protection Status" class="dmca-badge">
                                <img src ="https://www.btaskee.com/wp-content/uploads/2018/08/dmca-min.png"  alt="DMCA.com Protection Status" />
                            </a>
                        </div>
                        <a href='https://apps.apple.com/vn/app/btaskee-maids-and-cleaning/id1054302942?l=vi' target="_blank" rel='noopener noreferrer'>
                            <img
                                src={appStore}
                                alt='App Store'
                                width="120px"
                                className="d-inline my-2 me-2"
                                // height="30"
                            />
                        </a>
                        <a href='https://play.google.com/store/apps/details?id=com.lanterns.btaskee&hl=vi' target="_blank" rel='noopener noreferrer'>
                            <img
                                src={playStore}
                                alt='Play Store'
                                width="120px"
                                className="d-inline"
                                // height="30"
                            />
                        </a>
                        {/* <Row>
                            <Col>iOS</Col>
                            <Col>Android</Col>
                        </Row>
                        <Row>
                            <Col>iOS</Col>
                            <Col>Android</Col>
                        </Row> */}
                    </Col>
                    <Col xs={12} md={3}></Col>
                    <Col xs={12} md={3}>
                        <p className='fs-6 fw-bold'>Công ty</p>
                        <ul class="list-unstyled">
                            <li>
                                <a href="/gioi-thieu/">Giới thiệu</a>
                            </li>
                            <li>
                                <a href="#1">bRewards</a>
                            </li>
                            <li>
                                <a href="#2">bPay</a>
                            </li>
                            <li>
                                <a href="#3">Kinh nghiệm hay</a>
                            </li>
                        </ul>
                    </Col>
                    <Col xs={12} md={3}>
                        <p className='fs-6 fw-bold'>Dịch vụ</p>
                        <ul className="list-unstyled">
                            <li>
                                <a href="#4">Giúp việc nhà theo giờ</a>
                            </li>
                            <li>
                                <a href="#5">Tổng vệ sinh</a>
                            </li>
                            <li>
                                <a href="#6">Đi chợ</a>
                            </li>
                            <li>
                                <a href="#7">Vệ sinh máy lạnh</a>
                            </li>
                        </ul>
                    </Col>
                </Row>
                <Row className="align-items-end border-top border-secondary border-1 py-4 mt-5">
                    <Col xs={12} md={3}>
                        <p>© 2016 - 2021 bTaskee Co., Ltd.</p>
                    </Col>
                    <Col xs={12} md={3}>
                        {/* <p>FOLLOW US:</p> */}
                    </Col>
                    <Col xs={12} md={{ span: 3, offset: 3 }} className='text-end'>
                        <a href='http://online.gov.vn/Home/WebDetails/32665'>
                            <img width="120px" alt='' title='' src='http://online.gov.vn/Content/EndUser/LogoCCDVSaleNoti/logoSaleNoti.png'/>
                        </a>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Footer