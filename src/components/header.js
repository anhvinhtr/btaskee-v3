import logo from '../images/btaskee-logo-update.png'
import * as React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Navbar, Container, Nav, NavDropdown, Row, Col } from 'react-bootstrap'

const Header = () => {
    return (
        <nav>
            <Navbar bg="light" variant="light" expand="lg" fixed="top" collapseOnSelect style={{'font-weight':'500', 'border-bottom':'1px solid #dddddd'}}>
                <Container>
                    <Navbar.Brand href="/">
                        <img
                            src={logo}
                            alt='Logo bTaskee'
                            width="120px"
                            // height="30"
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav" style={{'margin-top':'5px'}}>
                        <Nav className="me-auto">
                            <NavDropdown title="Về bTaskee" id="nav-dropdown-about">
                                <NavDropdown.Item href="/gioi-thieu/">Giới thiệu</NavDropdown.Item>
                                <NavDropdown.Item href="/" disabled>Liên hệ</NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown title="Dịch vụ" id="nav-dropdown-service">
                                <Row>
                                    <Col xs="12" md="12" className="text-left">
                                        <NavDropdown.Item href="#" disabled>Giúp việc nhà theo giờ</NavDropdown.Item>
                                        <NavDropdown.Item href="#" disabled>Tổng vệ sinh</NavDropdown.Item>
                                        <NavDropdown.Item href="#" disabled>Đi chợ</NavDropdown.Item>
                                    </Col>
                                </Row>
                            </NavDropdown>
                            <Nav.Link href="/" disabled>bRewards</Nav.Link>
                            <Nav.Link href="/" disabled>bPay</Nav.Link>
                            <Nav.Link href="/kinh-nghiem-hay/" disabled>Kinh nghiệm hay</Nav.Link>
                        </Nav>
                        <Nav>
                            <NavDropdown title="Trở thành đối tác" id="nav-dropdown-partner">
                                <NavDropdown.Item href="/" disabled>Cộng tác viên giúp việc</NavDropdown.Item>
                                <NavDropdown.Item href="/" disabled>Cộng tác viên nấu ăn</NavDropdown.Item>
                                <NavDropdown.Item href="/" disabled>Đối tác dọn dẹp buồng phòng</NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown title="Tiếng Việt" id="nav-dropdown-language">
                                <NavDropdown.Item href="/th/" disabled>Thailand</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </nav>
    )
}

export default Header