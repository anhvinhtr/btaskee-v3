import * as React from 'react'
import Layout from '../components/layout'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../components/layout.module.css'
import { Link } from 'gatsby'
import { Container, Row, Col, Button } from 'react-bootstrap'

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import 'swiper/css';
import "swiper/css/pagination"
import "swiper/css/navigation"

// import Swiper core and required modules
import SwiperCore, {
	Autoplay,Pagination,Navigation
} from 'swiper';
  
// install Swiper modules
SwiperCore.use([Autoplay,Pagination,Navigation]);

const BannerHome = [
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2021/08/banner-khuyen-mai-30k-di-cho-pc-vie.jpeg',
		alt: 'First slide',
		link: '#'
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2021/01/download-app-btaskee-web-banner-ver3-vie.png',
		alt: 'Second slide',
		link: '#'
	},
	{
		src: 'http://www.btaskee.com/wp-content/uploads/2021/05/contact_tracing_banner_pc-vie.png',
		alt: 'Third slide',
		link: '#'
	}
];

const ServiceList = [
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/10/cong-tac-vien-.jpg',
		name: 'Giúp việc nhà theo giờ',
		text: 'Là dịch vụ đầu tiên bTaskee triển khai. Giờ đây công việc dọn dẹp không còn là nỗi bận tâm, bạn sẽ có nhiều thời gian nghỉ ngơi và tận hưởng cuộc sống.',
		url: '#1'
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/11/chon-dich-vu-deep-cleaning.png',
		name: 'Tổng vệ sinh',
		text: 'Xử lý chuyên sâu mọi vết bẩn trong căn nhà của bạn với từ 2 cộng tác viên giúp việc nhà trở lên.',
		url: '#2'
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/11/chon-dich-vu-grocery.png',
		name: 'Đi chợ',
		text: 'Việc mua sắm thực phẩm và đồ dùng gia đình trở nên tiện lợi hơn bao giờ hết. Giao hàng tận nơi chỉ sau 1h.',
		url: '#3'
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/11/chon-dich-vu-ac-cleaning.png',
		name: 'Vệ sinh máy lạnh',
		text: 'Giúp cải thiện chất lượng không khí, giảm mức tiêu thụ điện năng và tăng tuổi thọ máy lạnh tại nhà hay phòng làm việc của bạn.',
		url: '#4'
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/11/chon-dich-vu-laundry.png',
		name: 'Giặt ủi',
		text: 'Giúp bạn làm sạch quần áo nhanh chóng, cùng tiện ích giao nhận tận nơi.',
		url: '#5'
	}
];

const BenefitList = [
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/10/badge-time-clock.png',
		name: 'Đặt lịch nhanh chóng',
		text: 'Thao tác 60 giây trên ứng dụng, có ngay người nhận việc sau 60 phút.',
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/10/badge-pay.png',
		name: 'Giá cả rõ ràng',
		text: 'Giá dịch vụ được hiển thị rõ ràng trên ứng dụng. Bạn không phải trả thêm bất kỳ khoản chi phí nào.',
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/10/badge-service.png',
		name: 'Đa dạng dịch vụ',
		text: 'Với 9 dịch vụ tiện ích, bTaskee sẵn sàng hỗ trợ mọi nhu cầu việc nhà của bạn.',
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/10/badge-insurrance.png',
		name: 'An toàn tối đa',
		text: 'Người làm uy tín, luôn có hồ sơ lý lịch rõ ràng và được Công ty giám sát trong suốt quá trình làm việc.',
		url: '#4'
	}
];

const BenefitProcess = [
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/12/quy-trinh-su-dung-chon-dich-vu-ver3.png',
		name: 'Chọn dịch vụ',
		text: 'Chúng tôi có tới 9 dịch vụ tiện ích sẵn sàng hỗ trợ bạn.',
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2020/11/quy-trinh-su-dung-chon-thoi-gian-dia-diem.png',
		name: 'Chọn thời gian và địa điểm',
		text: 'Xác định ngày, giờ và địa điểm để đặt dịch vụ bTaskee trong vòng chưa đầy 60 giây. Bạn có thể tùy chọn số lần sử dụng theo nhu cầu.',
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2021/07/210721-quy-trinh-su-dung-tien-hanh-cong-viec.png',
		name: 'Tiến hành công việc',
		text: 'Người giúp việc gia đình/đối tác sẽ xác nhận đến nhà bạn như đã hẹn và thực hiện nhiệm vụ. Chất lượng, sự chuyên nghiệp luôn được đảm bảo 100%.',
	},
	{
		src: 'https://www.btaskee.com/wp-content/uploads/2021/07/210721-quy-trinh-su-dung-danh-gia-xep-hang.png',
		name: 'Đánh giá và xếp hạng',
		text: 'Bạn có thể đánh giá chất lượng dịch vụ thông qua ứng dụng bTaskee.',
	}
];


const HomePage = () => {
  return (
    <Layout pageTitle="Home Page">
		<div>
			<Swiper
				style={{
					'--swiper-navigation-color': '#FF8228',
					'--swiper-pagination-color': '#fff'
				}}
				autoplay={{
					"delay": 2500,
					"disableOnInteraction": false
				}} 
				loop={true}
				pagination={{"clickable": true}}
				navigation={true}
			>
				{BannerHome.map(slide => (
					<SwiperSlide>
						<img
							className="d-block w-100"
							src={slide.src}
							alt={slide.alt}
						/>
					</SwiperSlide>
				))}
			</Swiper>
		</div>
		<Container>
			<Row xs={1} md={3} className="my-3">
				<Col><h2>Tất cả những tiện ích gia đình mà bạn cần</h2></Col>
			</Row>
			<Row className="my-3">
				<Col>
					<Swiper
						style={{
							// '--swiper-navigation-color': '#FF8228',
							'--swiper-pagination-color': '#FF8228'
						}}
						slidesPerView={1} 
						spaceBetween={20}
						pagination={{"clickable": true}}
						// navigation={true}
						breakpoints={{
							"640": {
							  "slidesPerView": 2,
							  "spaceBetween": 20
							},
							"768": {
							  "slidesPerView": 3,
							  "spaceBetween": 40
							},
							"1024": {
							  "slidesPerView": 4,
							  "spaceBetween": 50
							}
						}}
					>
						{ServiceList.map(slide => (
							<SwiperSlide style={{'height':'500px'}}>
								<Link to={slide.url}>
									<img
										// className="d-block w-100"
										style={{'height':'200px', 'width':'300px'}}
										src={slide.src}
										alt={slide.name}
									/>
								</Link>
								<h3>{slide.name}</h3>
								<p style={{'height':'120px'}}>{slide.text}</p>
								<a className="link-custom" href={slide.url}>Tìm hiểu thêm</a>
							</SwiperSlide>
						))}
					</Swiper>
				</Col>
			</Row>
			<Row xs={1} md={2} className="my-3">
				<Col><h2>An tâm với lựa chọn của bạn</h2></Col>
			</Row>
			<Row className="my-3">
				<Col><img className="d-block w-100" src='https://www.btaskee.com/wp-content/uploads/2020/11/home-page-an-tam-voi-lua-chon-cua-ban.png' alt='An tâm với lựa chọn của bạn'/></Col>
			</Row>
			<Row xs={2} md={4} className="my-3">
				{BenefitList.map(slide => (
					<Col>
						<img 
							className="d-block w-25" 
							src={slide.src} 
							alt={slide.name}
						/>
						<h3>{slide.name}</h3>
						<p>{slide.text}</p>
					</Col>
				))}
			</Row>
			<Row xs={1} md={2} className="my-3">
				<Col><h2>Quy trình sử dụng dịch vụ</h2></Col>
			</Row>
			{BenefitProcess.map(slide => (
				<Row xs={1} md={3} className="justify-content-md-center align-items-center my-3">
					<Col>
						<img 
							className="d-block w-100"
							src={slide.src} 
							alt={slide.name}
						/>
					</Col>
					<Col>
						<h3>{slide.name}</h3>
						<p>{slide.text}</p>
					</Col>
				</Row>
			))}
		</Container>
		<div style={{'background-color':'#FF8228'}}>
			<Container>
				<Row xs={1} md={2} className="my-3">
					<Col>
						<h2 className='text-white'>Đăng ký ngay hôm nay</h2>
						<p className='text-white'>Bạn đã sẵn sàng trải nghiệm bTaskee chưa?<br/>Bắt đầu ngay với việc đặt lịch đầu tiên của bạn.</p>
						<Button href="#" className='mb-5' style={{'color':'#FF8228','backgroundColor':'white','border':'1px solid #FF8228'}}>Trải nghiệm dịch vụ</Button>
					</Col>
				</Row>
			</Container>
		</div>
    </Layout>
  )
}

export default HomePage