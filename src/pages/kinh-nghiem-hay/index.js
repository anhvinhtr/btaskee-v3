import * as React from 'react'
import { Link, graphql } from 'gatsby'
import Header from '../../components/header'

const BlogPage = ({ data }) => {
  return (
    <Header pageTitle="My Blog Posts">
        <ul>
        {
            data.allMdx.nodes.map((node) => (
                <article key={node.id}>
                    <h2>
                      <Link to={`/kinh-nghiem-hay/${node.slug}`}>
                      {node.frontmatter.title}
                      </Link>
                    </h2>
                    <p>Posted: {node.frontmatter.date}</p>
                </article>
            ))
        }    
        </ul>
    </Header>
  )
}

export const query = graphql`
  query {
    allMdx(sort: {fields: frontmatter___date, order: DESC}) {
      nodes {
        frontmatter {
          date(formatString: "MMMM D, YYYY")
          title
        }
        id
        slug
      }
    }
  }
`

export default BlogPage